sed -ie "s/%USERNAME_DEV%/$DB_DEV_USER/g" code/api/src/config/database.json
sed -ie "s/%PASSWORD_DEV%/$DB_DEV_PASS/g" code/api/src/config/database.json
sed -ie "s/%DATABASE_DEV%/$DB_DEV/g" code/api/src/config/database.json
sed -ie "s/%HOST_DEV%/$DB_DEV_ADDR/g" code/api/src/config/database.json
sed -ie "s/%USERNAME_PROD%/$DB_PROD_USER/g" code/api/src/config/database.json
sed -ie "s/%PASSWORD_PROD%/$DB_PROD_PASS/g" code/api/src/config/database.json
sed -ie "s/%DATABASE_PROD%/$DB_PROD/g" code/api/src/config/database.json
sed -ie "s/%HOST_PROD%/$DB_PROD_ADDR/g" code/api/src/config/database.json

