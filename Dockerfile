FROM node:10
RUN groupadd --gid 1001 app \
  && useradd --uid 1001 --gid 1001 --shell /bin/bash --create-home --home /home/app app
COPY --chown=app:app code /code
USER app
RUN cd /code/api && npm run setup && cd /code/web && npm run setup
RUN mkdir /code/web/public/js/bundles && chown -R app:app /code/web/public/js/bundles
WORKDIR /code/web/
EXPOSE 3000
ENTRYPOINT ["npm", "start"]%
